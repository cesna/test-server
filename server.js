const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const moment = require('moment');
moment.locale('lt');

const databaseUser = 'gica';
const databasePassword = 'persikas4all';
const databaseName = 'kazkas';
const databasePort = '19503';

const books = require('./routes/books.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
//db config
mongoose.connect(`mongodb://${databaseUser}:${databasePassword}@ds119503.mlab.com:${databasePort}/${databaseName}`, {useNewUrlParser: true});

app.get('/', (req, res) => res.send('Knygynas'));
app.use('/books', books);

app.listen(port, () => console.log(`Server eina ant ${port} porto!`));